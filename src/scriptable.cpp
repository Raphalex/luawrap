#include "scriptable.h"
#include <iostream>

Scriptable::Scriptable(const std::string script_path) : lua_table_(nullptr) {
  // Allocate the lua state
  this->lua_state_ = luaL_newstate();
  luaL_openlibs(this->lua_state_);
  // Read the Lua file
  if (luaL_dofile(this->lua_state_, script_path.c_str()) != 0) {
    std::cerr << "An error occured while loading the Lua script at path "
              << script_path << ": " << std::endl
              << lua_tostring(this->lua_state_, -1) << std::endl;
    exit(EXIT_FAILURE);
  }
  // Get actual file name (without the whole path)
  std::string table_name = script_path.substr(script_path.find_last_of("/") + 1,
                                              script_path.length());
  // Remove the .lua extension, and this is our script"s table name
  table_name = table_name.substr(0, table_name.length() - 4);
  this->lua_table_ = luabridge::getGlobal(this->lua_state_, table_name.c_str());
}