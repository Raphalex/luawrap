#include "luawrap.h"
#include <iostream>

class TestClass : public Scriptable {
public:
  TestClass() : Scriptable("tests/test.lua"){};
  int sum(int a, int b);

  void print_thing();
};

int TestClass::sum(int a, int b) {
  return this->call_lua_func_<int, int, int>("sum", a, b);
}

void TestClass::print_thing() { this->call_lua_func_("print_thing"); }

int main() {
  std::cout << "Hello world!" << std::endl;
  TestClass test;
  std::cout << "Result: " << test.sum(5, 5) << std::endl;
  test.print_thing();
}