#ifndef __SCRIPTABLE_H__
#define __SCRIPTABLE_H__

#include <functional>
#include <iostream>
#include <map>
#include <memory>

#include "lua_includes.h"

class Scriptable {
public:
  Scriptable(const std::string script_path);

protected:
  luabridge::LuaRef lua_table_;
  template <typename ret_type, typename... Types>
  ret_type call_lua_func_(const std::string &func_name, Types... args);
  template <typename... Types>
  void call_lua_func_(const std::string &func_name, Types... args);

private:
  lua_State *lua_state_ = nullptr;
};

/* Must implement this in the header for the linker to work */

template <typename ret_type, typename... Types>
ret_type Scriptable::call_lua_func_(const std::string &func_name,
                                    Types... args) {
  ret_type ret;
  auto lua_fun = this->lua_table_[func_name];
  if (lua_fun.isFunction()) {
    try {
      ret = lua_fun(args...);
    } catch (luabridge::LuaException const &e) {
      std::cerr << "Lua exception: " << e.what() << std::endl;
    }
    return ret;
  } else {
    std::cerr << "Function " << func_name << "is undefined in the Lua script"
              << std::endl;
    exit(EXIT_FAILURE);
  }
}

template <typename... Types>
void Scriptable::call_lua_func_(const std::string &func_name, Types... args) {
  auto lua_fun = this->lua_table_[func_name];
  if (lua_fun.isFunction()) {
    try {
      lua_fun(args...);
    } catch (luabridge::LuaException const &e) {
      std::cerr << "Lua exception: " << e.what() << std::endl;
    }
  } else {
    std::cerr << "Function " << func_name << "is undefined in the Lua script"
              << std::endl;
    exit(EXIT_FAILURE);
  }
}

#endif // __SCRIPTABLE_H__