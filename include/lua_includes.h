#ifndef __LUA_INCLUDES_H__
#define __LUA_INCLUDES_H__

extern "C" {
#include "lauxlib.h"
#include "lua.h"
#include "lualib.h"
}

#include "LuaBridge.h"

#endif // __LUA_INCLUDES_H__